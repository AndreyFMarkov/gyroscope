#include "stm32f30x.h"
#include "usart.h"
#include "crc.h"

static void (*USARTRxCB)(uint8_t*) = 0;

void USARTInit(void)
{
	__PCUSART_CLK_ENABLE; 
	__PCUSARTDMA_CLK_ENABLE;
	
  GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_StructInit(&GPIO_InitStructure);

  /* Configure Gyro pins: RX */
  GPIO_InitStructure.GPIO_Pin = PCUSART_RX_GPIO_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(PCUSART_RX_GPIO_PORT, &GPIO_InitStructure);
   
  /* Configure Gyro pins: TX */
  GPIO_InitStructure.GPIO_Pin = PCUSART_TX_GPIO_PIN;
  GPIO_Init(PCUSART_TX_GPIO_PORT, &GPIO_InitStructure);
   
  GPIO_PinAFConfig(PCUSART_RX_GPIO_PORT, PCUSART_RX_SOURCE, PCUSART_RX_AF);	
  GPIO_PinAFConfig(PCUSART_TX_GPIO_PORT, PCUSART_TX_SOURCE, PCUSART_TX_AF);
	
	USART_InitTypeDef USART_InitStructure;
	USART_StructInit(&USART_InitStructure);
	USART_InitStructure.USART_BaudRate = PCUSARTBAUDRATE;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_InitStructure.USART_Parity = USART_Parity_No;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_Init(PCUSART ,&USART_InitStructure);
	
	PCUSART->CR1 |= USART_CR1_RXNEIE;
	USART_Cmd(PCUSART, ENABLE);
	
	NVIC_EnableIRQ(PCUSARTIRQ);
	
	USART_DMACmd(PCUSART, USART_DMAReq_Tx, ENABLE);
	
	DMA_InitTypeDef DMA_InitStruct;
	DMA_StructInit(&DMA_InitStruct);
	DMA_InitStruct.DMA_DIR = DMA_DIR_PeripheralDST;
	DMA_InitStruct.DMA_M2M = DMA_M2M_Disable ;
	DMA_InitStruct.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t)&PCUSART->TDR;
	DMA_InitStruct.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStruct.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_Priority = DMA_Priority_Low;
	DMA_Init(PCUSARTTXDMACHANNEL ,&DMA_InitStruct);
	
	
}
	
void USARTDMATransmit(uint8_t* pBuf, uint8_t len)
{
	PCUSARTTXDMACHANNEL->CCR &= ~(DMA_CCR_EN);
	PCUSARTTXDMACHANNEL->CNDTR = len;
	PCUSARTTXDMACHANNEL->CMAR = (uint32_t)pBuf;
	PCUSARTTXDMACHANNEL->CCR |= DMA_CCR_EN;
}

void USARTSetRxCB(void (*CB)(uint8_t*))
{
	USARTRxCB = CB;
}

void PCUSARTIRQHANDLER
{
	static union
	{
		uint8_t 	buf[4];
		uint32_t 	shiftregister;
	} Rx;
	static uint8_t index = 0;
	Rx.buf[index++] = PCUSART->RDR;
	if(index == 3)
	{
		if(CRCCheck(Rx.buf, 2))
		{
			if(USARTRxCB)
				USARTRxCB(Rx.buf);
			index = 0;
		}
		else
		{
			Rx.shiftregister >>= 8;
			index = 2;
		}
	}
}