#pragma once

#define PCUSART									USART1
#define __PCUSART_CLK_ENABLE		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE)
#define __PCUSARTDMA_CLK_ENABLE	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE)

#define PCUSART_RX_GPIO_PORT		GPIOA
#define PCUSART_RX_GPIO_PIN			GPIO_Pin_10
#define PCUSART_RX_SOURCE       GPIO_PinSource10
#define PCUSART_RX_AF						GPIO_AF_7

#define PCUSART_TX_GPIO_PORT		GPIOA
#define PCUSART_TX_GPIO_PIN			GPIO_Pin_9
#define PCUSART_TX_SOURCE       GPIO_PinSource9
#define PCUSART_TX_AF						GPIO_AF_7

#define PCUSARTTXDMACHANNEL 		DMA1_Channel4

#define PCUSARTIRQ 				USART1_IRQn
#define PCUSARTIRQHANDLER 		USART1_IRQHandler(void)
#define PCUSARTBAUDRATE 		19200

void USARTInit(void);
void USARTSetRxCB(void (*CB)(uint8_t*));
void USARTDMATransmit(uint8_t* pBuf, uint8_t len);