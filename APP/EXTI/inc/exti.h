#pragma once

#define __SYSCFG_CLK_ENABLE           RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE)
#define TrigEXTI_Line                 EXTI_Line1
    
#define TrigEXTI_PIN                  GPIO_Pin_1                  
#define TrigEXTI_PORTSOURCE      			EXTI_PortSourceGPIOE 
#define TrigEXTI_PINSOURCE            EXTI_PinSource1

void EXTIInit(void);
void EXTIStart(void (*CB)(void));
void EXTIStop(void);