#include "stm32f30x.h"
#include "exti.h"

static void (*EXTI_CB)(void) = 0;

void EXTIInit(void)
{
	__SYSCFG_CLK_ENABLE;
	SYSCFG_EXTILineConfig(TrigEXTI_PORTSOURCE, TrigEXTI_PINSOURCE);
	NVIC_EnableIRQ(EXTI1_IRQn);
}
void EXTIStart(void (*CB)(void))
{
	EXTI_CB = CB;
	EXTI_InitTypeDef TrigEXTI;
	EXTI_StructInit(&TrigEXTI);
	TrigEXTI.EXTI_Line = TrigEXTI_Line;
	TrigEXTI.EXTI_LineCmd = ENABLE;
	TrigEXTI.EXTI_Mode = EXTI_Mode_Interrupt;
	TrigEXTI.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&TrigEXTI);
}

void EXTIStop(void)
{
	EXTI_InitTypeDef TrigEXTI;
	EXTI_StructInit(&TrigEXTI);
	TrigEXTI.EXTI_Line = TrigEXTI_Line;
	TrigEXTI.EXTI_LineCmd = DISABLE;
	EXTI_Init(&TrigEXTI);
}

void EXTI1_IRQHandler(void)
{
	EXTI->PR |= EXTI_PR_PR1;
	if(EXTI_CB)
		EXTI_CB();
}
