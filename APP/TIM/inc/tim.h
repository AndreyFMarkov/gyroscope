#pragma once

#define __MYTIM_CLK_ENABLE 	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE)
#define MYTIM 				TIM2
#define MYTIM_PRESCALER 	71
#define MYTIM_RR 			0xFFFFFFFF
void TIMInit(void);