#include "stm32f30x.h"
#include "tim.h"

void TIMInit(void)
{
	__MYTIM_CLK_ENABLE;
	TIM_TimeBaseInitTypeDef TS;
	TIM_TimeBaseStructInit(&TS);
	TS.TIM_Prescaler = MYTIM_PRESCALER;
	TS.TIM_CounterMode = TIM_CounterMode_Up ;
	TS.TIM_ClockDivision = TIM_CKD_DIV1;
	TS.TIM_Period = MYTIM_RR;
	TIM_TimeBaseInit(MYTIM, &TS);
	TIM_Cmd(MYTIM, ENABLE);
}