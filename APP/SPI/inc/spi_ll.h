#pragma once

#define GyroSPI                          SPI1
#define __GyroSPI_CLK_ENABLE             RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE)
    
#define GyroSPI_SCK_PIN                  GPIO_Pin_5                  
#define GyroSPI_SCK_GPIO_PORT            GPIOA                       
#define GyroSPI_SCK_GPIO_CLK             RCC_AHBPeriph_GPIOA
#define GyroSPI_SCK_SOURCE               GPIO_PinSource5
#define GyroSPI_SCK_AF                   GPIO_AF_5
 
#define GyroSPI_MISO_PIN                 GPIO_Pin_6                  
#define GyroSPI_MISO_GPIO_PORT           GPIOA                        
#define GyroSPI_MISO_GPIO_CLK            RCC_AHBPeriph_GPIOA
#define GyroSPI_MISO_SOURCE              GPIO_PinSource6
#define GyroSPI_MISO_AF                  GPIO_AF_5
 
#define GyroSPI_MOSI_PIN                 GPIO_Pin_7                 
#define GyroSPI_MOSI_GPIO_PORT           GPIOA                       
#define GyroSPI_MOSI_GPIO_CLK            RCC_AHBPeriph_GPIOA
#define GyroSPI_MOSI_SOURCE              GPIO_PinSource7
#define GyroSPI_MOSI_AF                  GPIO_AF_5
 
#define GyroSPI_CS_PIN                   GPIO_Pin_3                  
#define GyroSPI_CS_GPIO_PORT             GPIOE                      
#define GyroSPI_CS_GPIO_CLK              RCC_AHBPeriph_GPIOE


void SPILLInit(void);
_Bool SPILLTransfer(uint8_t length, uint8_t* pTxBuf, uint8_t* pRxBuf, void (*FinishCB)(void));
_Bool SPILL_isBSY(void);