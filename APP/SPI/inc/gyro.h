#pragma once

#define GYRO_FIFO_SIZE 30

void GYRO_Init(void);
_Bool GYRO_Start(uint8_t range, uint8_t odr);
void GYRO_Stop(void);
void Gyro_Poll(void);

