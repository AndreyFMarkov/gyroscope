#pragma once

typedef enum
    {                                               // DEFAULT    TYPE
      L3GD20_WHO_AM_I            = 0x0F,   // 11010100   r
      L3GD20_CTRL_REG1           = 0x20,   // 00000111   rw
      L3GD20_CTRL_REG2           = 0x21,   // 00000000   rw
      L3GD20_CTRL_REG3           = 0x22,   // 00000000   rw
      L3GD20_CTRL_REG4           = 0x23,   // 00000000   rw
      L3GD20_CTRL_REG5           = 0x24,   // 00000000   rw
      L3GD20_REFERENCE           = 0x25,   // 00000000   rw
      L3GD20_OUT_TEMP            = 0x26,   //            r
      L3GD20_STATUS_REG          = 0x27,   //            r
      L3GD20_OUT_X_L             = 0x28,   //            r
      L3GD20_OUT_X_H             = 0x29,   //            r
      L3GD20_OUT_Y_L             = 0x2A,   //            r
      L3GD20_OUT_Y_H             = 0x2B,   //            r
      L3GD20_OUT_Z_L             = 0x2C,   //            r
      L3GD20_OUT_Z_H             = 0x2D,   //            r
      L3GD20_FIFO_CTRL_REG       = 0x2E,   // 00000000   rw
      L3GD20_FIFO_SRC_REG        = 0x2F,   //            r
      L3GD20_INT1_CFG            = 0x30,   // 00000000   rw
      L3GD20_INT1_SRC            = 0x31,   //            r
      L3GD20_TSH_XH              = 0x32,   // 00000000   rw
      L3GD20_TSH_XL              = 0x33,   // 00000000   rw
      L3GD20_TSH_YH              = 0x34,   // 00000000   rw
      L3GD20_TSH_YL              = 0x35,   // 00000000   rw
      L3GD20_TSH_ZH              = 0x36,   // 00000000   rw
      L3GD20_TSH_ZL              = 0x37,   // 00000000   rw
      L3GD20_INT1_DURATION       = 0x38    // 00000000   rw
    } L3GD20Registers_t;

#define  L3GD20_READ_CMD               ((uint8_t)0x80)
#define  L3GD20_MULTIPLIE_CMD          ((uint8_t)0x40)


/******************* Bit definition for L3GD20_CTRL_REG1 register *****************/
#define  L3GD20_CTRL_REG1_DR1               ((uint8_t)0x80)
#define  L3GD20_CTRL_REG1_DR0               ((uint8_t)0x40)
#define  L3GD20_CTRL_REG1_BW0               ((uint8_t)0x20)
#define  L3GD20_CTRL_REG1_BW1               ((uint8_t)0x10)
#define  L3GD20_CTRL_REG1_ODR_95_12         ((uint8_t)0x00)
#define  L3GD20_CTRL_REG1_ODR_95_25         ((uint8_t)0x20)
#define  L3GD20_CTRL_REG1_ODR_190_12        ((uint8_t)0x40)
#define  L3GD20_CTRL_REG1_ODR_190_25        ((uint8_t)0x50)
#define  L3GD20_CTRL_REG1_ODR_190_50        ((uint8_t)0x60)
#define  L3GD20_CTRL_REG1_ODR_190_70        ((uint8_t)0x70)
#define  L3GD20_CTRL_REG1_ODR_380_20        ((uint8_t)0x80)
#define  L3GD20_CTRL_REG1_ODR_380_25        ((uint8_t)0x90)
#define  L3GD20_CTRL_REG1_ODR_380_50        ((uint8_t)0xA0)
#define  L3GD20_CTRL_REG1_ODR_380_100       ((uint8_t)0xB0)
#define  L3GD20_CTRL_REG1_ODR_760_30        ((uint8_t)0xC0)
#define  L3GD20_CTRL_REG1_ODR_760_35        ((uint8_t)0xD0)
#define  L3GD20_CTRL_REG1_ODR_760_50        ((uint8_t)0xE0)
#define  L3GD20_CTRL_REG1_ODR_760_100       ((uint8_t)0xF0)
#define  L3GD20_CTRL_REG1_ODR_Msk		    ((uint8_t)0xF0)
#define  L3GD20_CTRL_REG1_NPD               ((uint8_t)0x08)
#define  L3GD20_CTRL_REG1_ZEN               ((uint8_t)0x04)
#define  L3GD20_CTRL_REG1_XEN               ((uint8_t)0x02)
#define  L3GD20_CTRL_REG1_YEN               ((uint8_t)0x01)

/******************* Bit definition for L3GD20_CTRL_REG3 register *****************/
#define  L3GD20_CTRL_REG3_I1_INT1           ((uint8_t)0x80)
#define  L3GD20_CTRL_REG3_I1_BOOT           ((uint8_t)0x40)
#define  L3GD20_CTRL_REG3_H_LACTIVE         ((uint8_t)0x20)
#define  L3GD20_CTRL_REG3_PP_OD             ((uint8_t)0x10)
#define  L3GD20_CTRL_REG3_I2_DRDY           ((uint8_t)0x08)
#define  L3GD20_CTRL_REG3_I2_WTM            ((uint8_t)0x04)
#define  L3GD20_CTRL_REG3_ORUN              ((uint8_t)0x02)
#define  L3GD20_CTRL_REG3_EMPTY             ((uint8_t)0x01)

/******************* Bit definition for L3GD20_CTRL_REG4 register *****************/
#define  L3GD20_CTRL_REG4_BDU			          ((uint8_t)0x80)
#define  L3GD20_CTRL_REG4_BLE			`         ((uint8_t)0x40)
#define  L3GD20_CTRL_REG4_FS1					      ((uint8_t)0x20)
#define  L3GD20_CTRL_REG4_FS0		            ((uint8_t)0x10)
#define  L3GD20_CTRL_REG4_250DPS            ((uint8_t)0x00)
#define  L3GD20_CTRL_REG4_500DPS            ((uint8_t)0x10)
#define  L3GD20_CTRL_REG4_2000DPS   	      ((uint8_t)0x30)
#define  L3GD20_CTRL_REG4_RANGE_Msk  	      ((uint8_t)0x30)
#define  L3GD20_CTRL_REG4_SIM		 	          ((uint8_t)0x01)

/******************* Bit definition for L3GD20_CTRL_REG5 register *****************/
#define  L3GD20_CTRL_REG5_BOOT              ((uint8_t)0x80)
#define  L3GD20_CTRL_REG5_FIFO_EN           ((uint8_t)0x40)
#define  L3GD20_CTRL_REG5_HPEN     	        ((uint8_t)0x10)
#define  L3GD20_CTRL_REG5_INT1_SEL1         ((uint8_t)0x08)
#define  L3GD20_CTRL_REG5_INT1_SEL0         ((uint8_t)0x04)
#define  L3GD20_CTRL_REG5_OUT_SEL1          ((uint8_t)0x02)
#define  L3GD20_CTRL_REG5_OUT_SEL0          ((uint8_t)0x01)

/******************* Bit definition for L3GD20_STATUS_REG register *****************/
#define  L3GD20_STATUS_REG_ZYXOR	          ((uint8_t)0x80)
#define  L3GD20_STATUS_REG_ZOR   		        ((uint8_t)0x40)
#define  L3GD20_STATUS_REG_YOR       			  ((uint8_t)0x20)
#define  L3GD20_STATUS_REG_XOR           		((uint8_t)0x10)
#define  L3GD20_STATUS_REG_ZYXDA      		  ((uint8_t)0x08)
#define  L3GD20_STATUS_REG_ZDA           		((uint8_t)0x04)
#define  L3GD20_STATUS_REG_YDA             	((uint8_t)0x02)
#define  L3GD20_STATUS_REG_XDA           		((uint8_t)0x01)

/******************* Bit definition for L3GD20_FIFO_CTRL_REG register *****************/
#define  L3GD20_FIFO_CTRL_REG_FM2	          ((uint8_t)0x80)
#define  L3GD20_FIFO_CTRL_REG_FM1		        ((uint8_t)0x40)
#define  L3GD20_FIFO_CTRL_REG_FM0    			  ((uint8_t)0x20)
#define  L3GD20_FIFO_CTRL_REG_BYPASS 			  ((uint8_t)0x00)
#define  L3GD20_FIFO_CTRL_REG_FIFO   			  ((uint8_t)0x20)
#define  L3GD20_FIFO_CTRL_REG_STREAM 			  ((uint8_t)0x40)
#define  L3GD20_FIFO_CTRL_REG_STREAM2FIFO   ((uint8_t)0x60)
#define  L3GD20_FIFO_CTRL_REG_BYPASS2STREAM ((uint8_t)0x80)
#define  L3GD20_FIFO_CTRL_REG_WTM4       		((uint8_t)0x10)
#define  L3GD20_FIFO_CTRL_REG_WTM3     		  ((uint8_t)0x08)
#define  L3GD20_FIFO_CTRL_REG_WTM2       		((uint8_t)0x04)
#define  L3GD20_FIFO_CTRL_REG_WTM1         	((uint8_t)0x02)
#define  L3GD20_FIFO_CTRL_REG_WTM0       		((uint8_t)0x01)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL31 		((uint8_t)0x1F)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL30 		((uint8_t)0x1E)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL29 		((uint8_t)0x1D)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL28 		((uint8_t)0x1C)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL27 		((uint8_t)0x1B)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL26 		((uint8_t)0x1A)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL25 		((uint8_t)0x19)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL24 		((uint8_t)0x18)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL23 		((uint8_t)0x17)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL22 		((uint8_t)0x16)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL21 		((uint8_t)0x15)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL20 		((uint8_t)0x14)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL19 		((uint8_t)0x13)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL18 		((uint8_t)0x12)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL17 		((uint8_t)0x11)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL16  	((uint8_t)0x10)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL15 		((uint8_t)0x0F)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL14 		((uint8_t)0x0E)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL13 		((uint8_t)0x0D)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL12 		((uint8_t)0x0C)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL11 		((uint8_t)0x0B)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL10 		((uint8_t)0x0A)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL9  		((uint8_t)0x09)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL8  		((uint8_t)0x08)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL7  		((uint8_t)0x07)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL6  		((uint8_t)0x06)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL5  		((uint8_t)0x05)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL4  		((uint8_t)0x04)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL3  		((uint8_t)0x03)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL2  		((uint8_t)0x02)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL1  		((uint8_t)0x01)
#define  L3GD20_FIFO_CTRL_REG_WTMLEVEL_Msk	((uint8_t)0x1F)

/******************* Bit definition for L3GD20_FIFO_SRC_REG register *****************/
#define  L3GD20_FIFO_SRC_REG_WTM	          ((uint8_t)0x80)
#define  L3GD20_FIFO_SRC_REG_OVRN  		      ((uint8_t)0x40)
#define  L3GD20_FIFO_SRC_REG_EMPTY   			  ((uint8_t)0x20)
#define  L3GD20_FIFO_SRC_REG_DATA_Msk    		&((uint8_t)0x1F)
