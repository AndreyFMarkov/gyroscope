#include "stm32f30x.h"
#include "spi_ll.h"
#include "l3gd20.h"
#include "gyro.h"
#include "exti.h"
#include "crc.h"
#include "tim.h"
#include "exti.h"
#include "usart.h"

#define ODR_Pos 0
#define RANGE_Pos 1
#define GYRO_PREAMBLE 0xDEADBEAF

typedef __packed struct 
{
	uint32_t preamble;
	uint32_t Timestamp;
	uint16_t RxBuffer[GYRO_FIFO_SIZE];
	uint8_t crc;
} GYRO_Struct_t;

typedef struct
{
	_Bool Trigger;
	_Bool CmdReceived;
	uint8_t cmd[2];
} GYRO_StateStruct_t;

static GYRO_StateStruct_t GYRO_StateStruct = {0};
static GYRO_Struct_t GYRO_Struct = {0};

static void GYRO_SetTrigger(void)
{
	GYRO_Struct.Timestamp = MYTIM->CNT;
	GYRO_StateStruct.Trigger = 1;
}

static void GYRO_ReceiveCMD(uint8_t* pBuf)
{
	GYRO_StateStruct.cmd[0] = *pBuf++;
	GYRO_StateStruct.cmd[1] = *pBuf;
	GYRO_StateStruct.CmdReceived = 1;
}

static _Bool GYRO_WriteReg(L3GD20Registers_t address, uint8_t value)
{
	_Bool RetVal = 0;
	while(SPILL_isBSY());
	static uint8_t TxBuf[2];
	TxBuf[0] = (uint8_t)address;
	TxBuf[1] = (uint8_t)value;
	RetVal = SPILLTransfer(sizeof(TxBuf), TxBuf, 0, 0);
	return RetVal;
}
	
static _Bool GYRO_ReadZResult(uint8_t* pBuf)
{
	_Bool RetVal = 0;
	if(pBuf)
	{
		while(SPILL_isBSY());
		static uint8_t Buf[3];
		Buf[0] = (uint8_t)L3GD20_OUT_Z_L | L3GD20_READ_CMD | L3GD20_MULTIPLIE_CMD;
		RetVal = SPILLTransfer(3, Buf, Buf, 0);
		if(RetVal)
		{
			while(SPILL_isBSY());
			*pBuf++ = Buf[1];
			*pBuf = Buf[2];
		}
	}
	return RetVal;
}

void GYRO_Init(void)
{
	USARTSetRxCB(GYRO_ReceiveCMD);
	GYRO_Stop();
}

_Bool GYRO_Start(uint8_t range, uint8_t odr)
{
	_Bool RetVal = 1;
	RetVal &= GYRO_WriteReg(L3GD20_CTRL_REG5, L3GD20_CTRL_REG5_BOOT);
	RetVal &=GYRO_WriteReg(L3GD20_CTRL_REG3, L3GD20_CTRL_REG3_I1_BOOT | L3GD20_CTRL_REG3_I2_WTM );
	RetVal &=GYRO_WriteReg(L3GD20_FIFO_CTRL_REG, L3GD20_FIFO_CTRL_REG_STREAM|  L3GD20_FIFO_CTRL_REG_WTMLEVEL_Msk & GYRO_FIFO_SIZE);
	RetVal &=GYRO_WriteReg(L3GD20_CTRL_REG4, L3GD20_CTRL_REG4_BDU | L3GD20_CTRL_REG4_RANGE_Msk & range );
	RetVal &=GYRO_WriteReg(L3GD20_CTRL_REG5, L3GD20_CTRL_REG5_FIFO_EN);
	for(int i = 0; i < 32; i++) 					//clear fifo of the gyro
	{	uint8_t trash[2];
		RetVal &=GYRO_ReadZResult((uint8_t*)(trash));
	}
	RetVal &=GYRO_WriteReg(L3GD20_CTRL_REG1, L3GD20_CTRL_REG1_ODR_Msk & odr | L3GD20_CTRL_REG1_ZEN | L3GD20_CTRL_REG1_NPD);
	EXTIStart(GYRO_SetTrigger);
	return RetVal;
}
	
void GYRO_Stop(void)
{
	EXTIStop();
	GYRO_WriteReg(L3GD20_CTRL_REG1, 0);
}
	
void Gyro_Poll(void)
{
	if(GYRO_StateStruct.Trigger)
	{
		for(int i = 0; i < GYRO_FIFO_SIZE; i++)
			GYRO_ReadZResult((uint8_t*)(GYRO_Struct.RxBuffer + i));
		GYRO_Struct.preamble = GYRO_PREAMBLE;
		CRCAdd((uint8_t*)&GYRO_Struct, sizeof(GYRO_Struct_t) - 1);
		USARTDMATransmit((uint8_t*)&GYRO_Struct, sizeof(GYRO_Struct_t));
		GYRO_StateStruct.Trigger = 0;
	}
	if(GYRO_StateStruct.CmdReceived)
	{
		if(GYRO_StateStruct.cmd[ODR_Pos] & L3GD20_CTRL_REG1_NPD)
			GYRO_Start(GYRO_StateStruct.cmd[RANGE_Pos], GYRO_StateStruct.cmd[ODR_Pos]);
		else
			GYRO_Stop();
		GYRO_StateStruct.CmdReceived = 0;
	}
}