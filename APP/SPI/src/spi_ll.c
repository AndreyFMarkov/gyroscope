#include "stm32f30x.h"
#include "spi_ll.h"

typedef struct
{
  uint8_t* pRxBuf;
  uint8_t RemainCount;
	void (*FinishCB)(void);
	_Bool BSY;
} SPIStruct_t;

static SPIStruct_t SPIStruct = {0};

void SPILLInit(void)
{
  /* Gyro Periph clock enable */
  __GyroSPI_CLK_ENABLE; 

  GPIO_InitTypeDef GPIO_InitStructure;

  /* Configure Gyro pins: SCK */
  GPIO_InitStructure.GPIO_Pin = GyroSPI_SCK_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
  GPIO_Init(GyroSPI_SCK_GPIO_PORT, &GPIO_InitStructure);
   
  /* Configure Gyro pins: MISO */
  GPIO_InitStructure.GPIO_Pin = GyroSPI_MISO_PIN;
  GPIO_Init(GyroSPI_MISO_GPIO_PORT, &GPIO_InitStructure);
   
  /* Configure Gyro pins: MOSI */
  GPIO_InitStructure.GPIO_Pin = GyroSPI_MOSI_PIN;
  GPIO_Init(GyroSPI_MOSI_GPIO_PORT, &GPIO_InitStructure);

  /* Configure GyroCS_PIN pin: Gyro Card CS pin */
  GPIO_InitStructure.GPIO_Pin = GyroSPI_CS_PIN;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GyroSPI_CS_GPIO_PORT, &GPIO_InitStructure);
  GyroSPI_CS_GPIO_PORT->BSRR |= GyroSPI_CS_PIN;
   
  GPIO_PinAFConfig(GyroSPI_SCK_GPIO_PORT, GyroSPI_SCK_SOURCE, GyroSPI_SCK_AF);

  GPIO_PinAFConfig(GyroSPI_MISO_GPIO_PORT, GyroSPI_MISO_SOURCE, GyroSPI_MISO_AF); 

  GPIO_PinAFConfig(GyroSPI_MOSI_GPIO_PORT, GyroSPI_MOSI_SOURCE, GyroSPI_MOSI_AF);  

  SPI_InitTypeDef SPI_InitStructure;

  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
  SPI_InitStructure.SPI_CRCPolynomial = 0;
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;

  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_Init(GyroSPI, &SPI_InitStructure);
  SPI_RxFIFOThresholdConfig(GyroSPI, SPI_RxFIFOThreshold_QF);
  SPI_Cmd(GyroSPI, ENABLE);
  //SPI_DataSizeConfig(GyroSPI, ENABLE);
  
  SPI_I2S_ITConfig(GyroSPI, SPI_I2S_IT_RXNE, ENABLE);
  NVIC_EnableIRQ(SPI1_IRQn);
}

_Bool SPILL_isBSY(void)
{
	return SPIStruct.BSY;
}

_Bool SPILLTransfer(uint8_t length, uint8_t* pTxBuf, uint8_t* pRxBuf, void (*FinishCB)(void))
{
  if(SPIStruct.RemainCount
		 	|| length == 0
			|| pTxBuf == 0)
    return 0;
	if(length > 4)
		length = 4;
	SPIStruct.BSY = 1;
  GyroSPI_CS_GPIO_PORT->BRR |= GyroSPI_CS_PIN;
  SPIStruct.RemainCount = length;
  SPIStruct.pRxBuf = pRxBuf;
	SPIStruct.FinishCB = FinishCB;
  for(int i = 0; i < length; ++i)
    GyroSPI->DR8 =  *pTxBuf++;
  return 1;
}

void SPI1_IRQHandler(void)
{
  uint8_t rxd = GyroSPI->DR8;
  if(SPIStruct.RemainCount)
  {
		if(SPIStruct.pRxBuf != 0)
    	*SPIStruct.pRxBuf++ = rxd;
    if(--SPIStruct.RemainCount == 0)
		{
      GyroSPI_CS_GPIO_PORT->BSRR |= GyroSPI_CS_PIN;
			if(SPIStruct.FinishCB)
				SPIStruct.FinishCB();
			SPIStruct.BSY = 0;
		}
  }
}