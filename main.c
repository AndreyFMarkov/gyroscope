#include "stm32f30x.h"
#include "gpio.h"
#include "spi_ll.h"
#include "l3gd20.h"
#include "gyro.h"
#include "tim.h"
#include "usart.h"
#include "exti.h"


void main()
{
	TIMInit();
	GPIOInit();
	USARTInit();
  	EXTIInit();
	SPILLInit();
	GYRO_Init();
	while(1)
  {
		Gyro_Poll();
  }
}
